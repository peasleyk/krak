package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"unicode"
)

// Choose a tolerance that guarantees each rune is mapped to another. Longer texts can use greater tolerances
const TOLERANCE = .0005

type frequency struct {
	freq map[rune]float64
}

type info struct {
	char rune
	diff float64
}

func getEnglish() frequency {
	english := frequency{}
	english.freq = make(map[rune]float64)
	english.freq['a'] = .08167
	english.freq['b'] = .01492
	english.freq['c'] = .02782
	english.freq['d'] = .04253
	english.freq['e'] = .12702
	english.freq['f'] = .02228
	english.freq['g'] = .02015
	english.freq['h'] = .06094
	english.freq['i'] = .06966
	english.freq['j'] = .00153
	english.freq['k'] = .00772
	english.freq['l'] = .04025
	english.freq['m'] = .02406
	english.freq['n'] = .06749
	english.freq['o'] = .07507
	english.freq['p'] = .01929
	english.freq['q'] = .00095
	english.freq['r'] = .05987
	english.freq['s'] = .06327
	english.freq['t'] = .09056
	english.freq['u'] = .02758
	english.freq['v'] = .00978
	english.freq['w'] = .02360
	english.freq['x'] = .00150
	english.freq['y'] = .01974
	english.freq['z'] = .00074
	return english
}

// Creates a map of characters to their count from a reader
func charFrequency(r bufio.Reader) frequency {
	dist := make(map[rune]int)
	var total int
	for {
		r, _, err := r.ReadRune()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic("Error reading character in ciphertext")
		}
		if r >= 'A' && r <= 'z' {
			dist[unicode.ToLower(r)]++
			total++
		}
	}

	fr := frequency{}
	fr.freq = make(map[rune]float64)
	for k, v := range dist {
		fr.freq[k] = float64(float64(v) / float64(total))
	}
	return fr
}

// Tries to find a mapping between two frequency distributions
func findMapping(cipher frequency, language frequency) map[rune][]info {
	conv := make(map[rune][]info)
	for k, _ := range cipher.freq {
		for k2, _ := range language.freq {
			diff := math.Abs(cipher.freq[k] - language.freq[k2])
			if diff <= TOLERANCE {
				entry := info{
					char: k2,
					diff: diff,
				}
				conv[k] = append(conv[k], entry)
			}
		}
	}
	return conv
}

func main() {
	file := flag.String("file", "example.txt", "cipher .txt file")
	flag.Parse()

	f, err := os.Open(*file)
	defer f.Close()
	if err != nil {
		panic("Can't read file")
	}

	reader := bufio.NewReader(f)
	fr := charFrequency(*reader)
	english := getEnglish()
	mapping := findMapping(fr, english)

	fmt.Println("Possible Cipher character to English character mappings")
	for k, v := range mapping {
		fmt.Printf("\nCipher : %s\n", string(k))
		for i := 0; i < len(v); i++ {
			fmt.Printf("English: %s Diff: %2f\n", string(v[i].char), v[i].diff)
		}
	}

}
