# Krak

Simple program to help decipher substitution ciphers

Makes use of frequency leakage to generate possible substitutions that would make sense, based on the English language.

Messing with tolerance may give you more choices on a smaller cipher text

## Usage

```shell
Usage of ./krak:
  -file string
      Ciphered txt file (default "example.txt")
```

This will print out a list of cipher text characters to possible plaintext characters

## Example output on the cipher

The output is a list of possible english characters for the cipher text character given the difference in frequency was below the threshold. If the tolerance was too low or the cipher too small, there may be less mappings
```shell
$ /krac -file cipher.txt
Possible Cipher character to English character mappings

Cipher : v
English: j Diff: 0.000162
English: q Diff: 0.000742
English: x Diff: 0.000192
English: z Diff: 0.000952

Cipher : o
English: a Diff: 0.000452

Cipher : f
English: o Diff: 0.000620

Cipher : j
English: j Diff: 0.000684
English: q Diff: 0.000104
English: x Diff: 0.000654
English: z Diff: 0.000106

Cipher : i
English: d Diff: 0.000229

Cipher : z
English: b Diff: 0.000308

Cipher : x
English: b Diff: 0.000538

Cipher : d
English: w Diff: 0.000757
English: f Diff: 0.000563

Cipher : c
English: m Diff: 0.000371
English: w Diff: 0.000089

Cipher : u
English: p Diff: 0.000677

Cipher : h
English: b Diff: 0.000538

Cipher : s
English: l Diff: 0.000487
```